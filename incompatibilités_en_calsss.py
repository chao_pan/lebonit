import pandas as pd
import time

class Incompatibilité():
    def __init__ (self, pathInput, pathOutput):
        self.allsegments = pd.read_excel(pathInput)
        self.zone_isolée = {}
        self.iti_incom = {}
        self.sig = 0
        self.segmentDeleted = []
        self.incom = pd.DataFrame(columns = ['Its', 'Incom'])
        self.pathOutput = pathOutput
        self.itiDirectionAbnormal = []
    def GetZoneIsonée(self):
        for i in range(self.allsegments.shape[0]):
            if ';;' in self.allsegments.loc[i, self.allsegments.columns[1]]:
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]].replace(';;', ';')    
            if self.allsegments.loc[i, self.allsegments.columns[1]][0] == ';':
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]][1:]
            if self.allsegments.loc[i, self.allsegments.columns[1]][-1] == ';':
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]][:-1]
        for i in range(self.allsegments.shape[0]):
            line = []
            for j in self.allsegments.iloc[i, 1].split(';'):
                if 'Z_' in j:
                    if len(j.split('_')) == 4:
                        line.append(j.split('_')[0] + '_' + j.split('_')[1] + '_' + j.split('_')[2])
                    elif len(j.split('_')) == 3:
                        line.append(j.split('_')[0] + '_' + j.split('_')[1])
                    else:
                        print(i+2)
            self.zone_isolée[self.allsegments.iloc[i, 0]] = list(set(line))
    def IsSuperpositionBetweenTwoListZoneIsonée(self, itiA, itiB):
        for zoneisolée in itiA:
            if zoneisolée in itiB:
                return 1 #at least one superposition
                break
        return 0 # no superposition
    def GetItiIncom(self): #First step to get all incompatibities in terms of zone isolée
        for i in range(self.allsegments.shape[0]):
            line = []
            for j in range(self.allsegments.shape[0]):
                len1 = len(self.zone_isolée[self.allsegments.iloc[i, 0]]) + len(self.zone_isolée[self.allsegments.iloc[j, 0]])
                len2 = len(set(self.zone_isolée[self.allsegments.iloc[i, 0]] + self.zone_isolée[self.allsegments.iloc[j, 0]]))
                if self.IsSuperpositionBetweenTwoListZoneIsonée(self.zone_isolée[self.allsegments.iloc[i, 0]], self.zone_isolée[self.allsegments.iloc[j, 0]]) == 1:
                    line.append(self.allsegments.iloc[j, 0])
            self.iti_incom[self.allsegments.iloc[i, 0]] = line
    def countNumberSuperpositionOfZoneIsolée(self, tableau1, tableau2):
        self.sig = 0
        for i in tableau1:
            if i in tableau2:
                self.sig += 1
    def FilterSegmentDeleted(self, OneIti, HisIncom):
        self.segmentDeleted = []
        for i in HisIncom:
            if OneIti.split('_')[0] == i.split('_')[1] or OneIti.split('_')[1] == i.split('_')[0]:
                self.countNumberSuperpositionOfZoneIsolée(self.zone_isolée[OneIti], self.zone_isolée[i])
                if self.sig ==1:
                    if OneIti not in self.itiDirectionAbnormal:
                        if int(OneIti.split('_')[0]) < int(OneIti.split('_')[1]):
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                        else:
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                    else:
                        if int(OneIti.split('_')[0]) < int(OneIti.split('_')[1]):
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                        else:
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])):
                                self.segmentDeleted.append(i)                    
    def AmeliorateIncom(self):
        self.VerifyItiDirection()
        for i in self.iti_incom.keys():
            self.FilterSegmentDeleted(i, self.iti_incom[i])
            for j in self.segmentDeleted:
                self.iti_incom[i].remove(j)
    def WriteToExcel(self):
        for i in self.iti_incom.keys():
            self.incom.loc[i, 'Its'] = i
            for j in self.iti_incom[i]:
                if pd.isna(self.incom.loc[i, 'Incom']):
                    self.incom.loc[i, 'Incom'] = j
                else:
                    self.incom.loc[i, 'Incom'] = self.incom.loc[i, 'Incom'] + ';' + j
        self.incom.to_excel(pathOutput, sheet_name = 'it_ZEP', index = False)
    def VerifyItiDirection(self):
        coor = pd.read_excel('C:/CHAO PAN/Gares/Paris Montparnasse/PMP_coords.xlsx', sheet_name = 'carres')
        coor_carre = {}
        itiPrefix = {}
        self.itiDirectionAbnormal = []
        for i in range(coor.shape[0]):
            if pd.notna(coor.iloc[i, 1]) and ',' not in coor.iloc[i, 1] and 'Sheet' in coor.iloc[i, 0]:
                coor_carre[coor.iloc[i, 1]] = coor.loc[i][2:]
        for i in range(self.allsegments.shape[0]):
            pre = str(self.allsegments.iloc[i, 0].split('_')[0])
            suf = str(self.allsegments.iloc[i, 0].split('_')[1])
            p = s = ''
            if len(self.allsegments.iloc[i, 0].split('_')) == 3:
                jal = str(self.allsegments.iloc[i, 0].split('_')[2])
            else:
                jal = ''
            for prefix in ['CV', 'C', 'F']:
                if p == "": 
                    try:
                        p = coor.loc[coor['Shape_Text'] == prefix + pre, 'Shape_Text'].values[0]
                    except:
                        p = ''
                if s == "":
                    try:
                        s = coor.loc[coor['Shape_Text'] == prefix + suf, 'Shape_Text'].values[0]
                    except:
                        s = ''
            if jal == '':
                if p != '' and s != '':
                    itiPrefix[self.allsegments.iloc[i, 0]] = p + '_' + s
                else:
                    itiPrefix[self.allsegments.iloc[i, 0]] = ''
            else:
                if p != '' and s != '':
                    itiPrefix[self.allsegments.iloc[i, 0]] = p + '_' + s + '_' + jal
                else:
                    itiPrefix[self.allsegments.iloc[i, 0]] = ''
        for i in itiPrefix.keys():
            if itiPrefix[i] != "":
                pre = str(itiPrefix[i].split('_')[0])
                suf = str(itiPrefix[i].split('_')[1])
                preN = int(i.split('_')[0])
                sufN = int(i.split('_')[1])
                if (coor_carre[pre][0] - coor_carre[suf][0])*(preN - sufN) < 0:
                    self.itiDirectionAbnormal.append(i)

    def CarryOut(self):
        self.GetZoneIsonée()
        self.GetItiIncom()
        self.AmeliorateIncom()
        self.WriteToExcel()

if __name__ == '__main__':
    time1 = time.time()
    pathInput = 'C:/CHAO PAN/Gares/Paris Montparnasse/IT_allsegments_15102018.xlsx'
    pathOutput = "C:/CHAO PAN/Gares/Paris Montparnasse/incom.xlsx"
    incom = Incompatibilité(pathInput, pathOutput)
    incom.CarryOut()
    time2 = time.time()
    print('Finished with {}s'.format(round(time2-time1, 2)))













































