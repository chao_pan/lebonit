import pandas as pd
import math
import xlsxwriter
import time
import operator

def Distance(p1, p2):
    return math.sqrt(math.pow(p1[0] - p2[0], 2) + math.pow(p1[1] - p2[1], 2))

def Set_coordinate():
    for i in coor.keys():
        if coor[i][0] > coor[i][2]:
            tempx = coor[i][2]
            coor[i][2] = coor[i][0]
            coor[i][0] = tempx
            tempy = coor[i][3]
            coor[i][3] = coor[i][1]
            coor[i][1] = tempy
        if coor[i][0] == coor[i][2]:
            if coor[i][1] > coor[i][3]:
                tempy = coor[i][1]
                coor[i][1] = coor[i][3]
                coor[i][3] = tempy
        #objet_stange = ['AG_97_G', 'Z_163_03', 'Z_154_03', 'AG_98_G', 'Z_153_03', 'AG_99_G', 'AG_145_G', 'Z_218_05', 'Z_213_02', 'AG_146_G', 'AG_158_G', 'Z_219_11', 'Z_221_02', 'AG_159_G']
        else: 
            k = abs(coor[i][1] - coor[i][3])/abs(coor[i][0] - coor[i][2])
            if k > 212.9: # here we must determine this value 212.9
                if coor[i][1] > coor[i][3]:
                    tempy = coor[i][1]
                    coor[i][1] = coor[i][3]
                    coor[i][3] = tempy
                    tempx = coor[i][0]
                    coor[i][0] = coor[i][2]
                    coor[i][2] = tempx

def Collection_min_distance(): #collect all mini distances for each group segment and its nearest neighbor
    min_dis = []
    for i in coor.keys():
        dis = []
        for j in coor.keys():
            if i != j:
                dis.append(dis_2_seg[(i, j)]['d10'])
        min_dis.append(min(dis))
    return min_dis

def Nearest_Neighbor(obj):
    threshold = 3.61
    NN = {}
    for i in coor.keys():
        if i == obj:
            continue
        dis = dis_2_seg[(obj, i)]['d10']
        if dis < threshold:
            while(dis in NN.keys()):
                dis += 0.00001
            NN[dis] = i
    first_min_dis = min(NN.keys())
    dis_without_first_min = []
    for i in NN.keys():
        if i != first_min_dis:
            dis_without_first_min.append(i)
    second_min_dis = min(dis_without_first_min)
    
    if 'Z_' in obj or 'z_' in obj:
        if ('AG_' in NN[first_min_dis] or 'TJD_' in NN[first_min_dis] or 'TJS_' in NN[first_min_dis]) \
                and ('AG_' in NN[second_min_dis] or 'TJD_' in NN[second_min_dis] or 'TJS' in NN[second_min_dis]):
            if dis_2_seg[(NN[first_min_dis], NN[second_min_dis])]['d00'] < dis_2_seg[(NN[first_min_dis], NN[second_min_dis])]['d11']:
                return [NN[first_min_dis], NN[second_min_dis]]
            else:
                return NN[first_min_dis]
        elif ('Z_' in NN[second_min_dis] or 'z_' in NN[second_min_dis]) and ('AG_' in NN[first_min_dis] or 'TJD_' in NN[first_min_dis] or 'TJS' in NN[first_min_dis]):
            return NN[first_min_dis]
        elif ('AG_' in NN[second_min_dis] or 'TJD_' in NN[second_min_dis] or 'TJS' in NN[second_min_dis]) and ('Z_' in NN[first_min_dis] or 'z_' in NN[first_min_dis]):
            distance_self = Distance((coor[NN[first_min_dis]][0], coor[NN[first_min_dis]][1]), (coor[NN[first_min_dis]][2], coor[NN[first_min_dis]][3]))
            if dis_2_seg[(obj, NN[second_min_dis])]['d10'] > 0.9*distance_self: #e.g. Z_2444_02
                return NN[first_min_dis]
            else:
                if dis_2_seg[(obj, NN[second_min_dis])]['d10'] > dis_2_seg[(obj, NN[second_min_dis])]['d11']: #e.g. Z_2136_05
                    return NN[first_min_dis]
                else: #e.g. Z_76_01
                    if abs(coor[obj][3] - coor[NN[first_min_dis]][3]) < abs(coor[obj][3] - coor[NN[second_min_dis]][3]):
                        return NN[first_min_dis]
                    else:
                        return NN[second_min_dis]
        else:
            return NN[first_min_dis]
    else:
        if ('TJD_' in NN[first_min_dis] or 'TJS_' in NN[first_min_dis]) and ('TJD_' in NN[second_min_dis] or 'TJS_' in NN[second_min_dis]):
            if obj[-1] == NN[second_min_dis][-1]:
                if dis_2_seg[(NN[first_min_dis], NN[second_min_dis])]['d00'] < dis_2_seg[(NN[first_min_dis], NN[second_min_dis])]['d11']:
                    return NN[second_min_dis]
                else:
                    return NN[first_min_dis]
            else:
                return NN[first_min_dis]
        elif ('Z_' in NN[second_min_dis] or 'z_' in NN[second_min_dis]) and ('AG_' in NN[first_min_dis]):
            if dis_2_seg[(obj, NN[first_min_dis])]['d10'] < dis_2_seg[(obj, NN[first_min_dis])]['d11']:
                return NN[second_min_dis]
        else:
            return NN[first_min_dis]

def Get_nearest_segment_for_carre (obj, sig):
    dis = {}
    center_x = coor_carre[obj][0]
    center_y = coor_carre[obj][1]
    if sig == True: #if this object is a prefix, we take its begin coordinate
        for i in coor.keys():
            seg_x = coor[i][0]
            seg_y = coor[i][1]
            d = math.sqrt(math.pow((center_x - seg_x), 2) + math.pow((center_y - seg_y), 2))
            while(d in dis.keys()):
                d += 0.00001
            dis[d] = i
    else: #if this object is a suffix, we take its end coordinate
        for i in coor.keys():
            seg_x = coor[i][2]
            seg_y = coor[i][3]
            d = math.sqrt(math.pow((center_x - seg_x), 2) + math.pow((center_y - seg_y), 2))
            while(d in dis.keys()):
                d += 0.00001
            dis[d] = i
    while ('Z_' not in dis[min(dis.keys())]):
        del dis[min(dis.keys())]
    return dis[min(dis.keys())]

def Get_nearest_segment_for_jalon (obj):
    dis = {}
    center_x = coor_jalon[obj][0] + coor_jalon[obj][2] / 2
    center_y = coor_jalon[obj][1] + coor_jalon[obj][3] / 2
    for i in coor.keys():
        seg_x = (coor[i][0] + coor[i][2]) / 2
        seg_y = (coor[i][1] + coor[i][3]) / 2
        d = math.sqrt(math.pow((center_x - seg_x), 2) + math.pow((center_y - seg_y), 2))
        while(d in dis.keys()):
            d += 0.00001
        dis[d] = i
    while('Z_' not in dis[min(dis.keys())]):
        del dis[min(dis.keys())]
    return dis[min(dis.keys())]

def Get_nearest_segment_for_canton (obj):
    dis = {}
    center_x = coor_canton[obj][0] + coor_canton[obj][2] / 2
    center_y = coor_canton[obj][1] + coor_canton[obj][3] / 2
    for i in coor.keys():
        seg_x = (coor[i][0] + coor[i][2]) / 2
        seg_y = (coor[i][1] + coor[i][3]) / 2
        d = math.sqrt(math.pow((center_x - seg_x), 2) + math.pow((center_y - seg_y), 2))
        while(d in dis.keys()):
            d += 0.00001
        dis[d] = i
    while('Z_' not in dis[min(dis.keys())]):
        del dis[min(dis.keys())]
    return dis[min(dis.keys())]

def Next_segment(obj):
    return Nearest_Neighbor(obj)

def Find_last_double_segment(ind, segment):
    for i in list(ind.keys())[::-1]:
        if len(ind[i]) == 2:
            del ind[i]
            segment = segment[:-1]
            return ind, segment, i
        else:
            del ind[i]
            segment = segment[:-1]
    #return ind, segment, i

def Find_way(seg1, seg2):
    distance = dis_2_seg[(seg1, seg2)]['d01']
    segment = []
    ind = {}
    sig = 0
    seg = seg1
    count = 0
    while (1):
        count += 1
        if count > 1000:
            return set(segment)
            break
        dis = Distance((coor[seg1][2], coor[seg1][3]), (coor[seg][0], coor[seg][1]))
        if dis > 1.5*distance or seg in segment:
            segment.append(seg)
            try:
                ind, segment, seg = Find_last_double_segment(ind, segment)
            except:
                return []
                break
            ind[seg] = seg_to_seg[seg][1]
            seg = seg_to_seg[seg][1]
            sig = 1
            count = 0
        if seg == seg2:
            segment.append(seg)
            return segment
            break
        if sig == 1:
            sig = 0
        else:
            segment.append(seg)
            if len(seg_to_seg[seg]) == 2:
                ind[seg] = seg_to_seg[seg]
                seg = seg_to_seg[seg][0]
            else:
                ind[seg] = seg_to_seg[seg]
                seg = seg_to_seg[seg]

def ReadItiName():
    pathIts =  'C:/CHAO PAN/Gares/Paris Est/s9c pro/IT_allsegments_PE_origine.xlsx'
    its =  pd.read_excel(pathIts)
    it = {}
    for i in range(its.shape[0]):
        if pd.notna(its.loc[i, 'Its dans visio']):
            iti = its.loc[i, 'Its dans visio']
            if iti.split('_')[0] in coor_carre.keys() and iti.split('_')[1] in coor_carre.keys():
                it[its.loc[i, 'Its']] = (its.loc[i, 'Its dans visio'], its.iloc[i, 3])
        else:
            iti = its.loc[i, 'Its']
            if iti.split('_')[0] in coor_carre.keys() and iti.split('_')[1] in coor_carre.keys():
                it[its.loc[i, 'Its']] = its.loc[i, 'Its']
    return it.values()

def All_itinéraire(iti):
    itinéraire = {}
    for i in iti:
        pre = str(i.split('_')[0])
        suf = str(i.split('_')[1])
        if len(i.split('_')) == 3:
            jalon = str(i.split('_')[2])
            if jalon not in coor_jalon.keys():
                jalon = ''
        else:
            jalon = ''
        if coor_carre[pre][0] > coor_carre[suf][0]:
            temp = pre
            pre = suf
            suf = temp
        segment_pre = Get_nearest_segment_for_carre(pre, 1)
        segment_suf = Get_nearest_segment_for_carre(suf, 0)
        if segment_pre == segment_suf:
            segments = [segment_pre]
        else:
            if jalon != '':
                seg_jal = Get_nearest_segment_for_jalon(jalon)
                segments = Find_way(segment_pre, seg_jal) + Find_way(seg_jal, segment_suf)[1:]
            else:
                segments = Find_way(segment_pre, segment_suf)
        line = ''
        for j in segments:
            if line == '':
                line = j
            else:
                line = line + ';' + j
        itinéraire[i] = line
    return itinéraire

def export_all_itinéraire(itinéraire):
    path = 'C:/CHAO PAN/Gares/Paris Est/s9c pro/IHM/IT_allsegments_PE.xlsx'
    wb = xlsxwriter.Workbook(path)
    ws = wb.add_worksheet('All_Segments')
    ws.write(0, 0, 'Its')
    ws.write(0, 1, 'Allsegments')
    k = 1
    for i in itinéraire.keys():
        ws.write(k, 0, i)
        ws.write(k, 1, itinéraire[i])
        k += 1
    wb.close()

def forException():
    iti =  pd.read_excel('C:/CHAO PAN/Gares/Paris Est/s9c pro/IT_allsegments_PE_manquants.xlsx', sheet_name = 'All_Segments')
    iti['segofCanton'] = pd.Series()
    iti['allsegment'] = pd.Series()
    for i in range(iti.shape[0]):
        if pd.notna(iti.iloc[i, 3]):
            if ',' in iti.iloc[i, 3]:
                line = {}
                for j in iti.iloc[i, 3].split(','):
                    if j in coor_jalon.keys():
                        line[Get_nearest_segment_for_jalon(j)] = coor[Get_nearest_segment_for_jalon(j)][0]
                newline = sorted(line.items(), key = operator.itemgetter(1), reverse = False)
                for j in newline:
                    if pd.isna(iti.iloc[i, 4]):
                        iti.iloc[i, 4] = j[0]
                    else:
                        iti.iloc[i, 4] = iti.iloc[i, 4] + ';' + j[0]
                pre = str(iti.iloc[i, 2].split('_')[0])
                suf = str(iti.iloc[i, 2].split('_')[1])
                for j in forExceptionFindWay(pre, suf, iti.loc[i, 'segofCanton']):
                    if pd.isna(iti.loc[i, 'allsegment']):
                        iti.loc[i, 'allsegment'] = j
                    else:
                        iti.loc[i, 'allsegment'] = iti.loc[i, 'allsegment'] + ';' + j
            else:
                if iti.iloc[i, 3] in coor_jalon.keys():
                    iti.iloc[i, 4] = Get_nearest_segment_for_jalon(iti.iloc[i, 3])
                else:
                    iti.iloc[i, 4] = iti.iloc[i, 3]
                pre = str(iti.iloc[i, 2].split('_')[0])
                suf = str(iti.iloc[i, 2].split('_')[1])
                for j in forExceptionFindWay(pre, suf, iti.loc[i, 'segofCanton']):
                    if pd.isna(iti.loc[i, 'allsegment']):
                        iti.loc[i, 'allsegment'] = j
                    else:
                        iti.loc[i, 'allsegment'] = iti.loc[i, 'allsegment'] + ';' + j
        
    iti.to_excel('C:/CHAO PAN/Gares/Paris Est/s9c pro/1.xlsx', index = False)

def forExceptionFindWay(pre, suf, jalon):
    if pre in coor_canton.keys() and suf in coor_carre.keys():
        if coor_canton[pre][0] > coor_carre[suf][0]:
            seg_pre = Get_nearest_segment_for_carre(suf, True)
            seg_suf = Get_nearest_segment_for_carre(pre, False)
        else:
            seg_pre = Get_nearest_segment_for_carre(pre, True)
            seg_suf = Get_nearest_segment_for_carre(suf, False)
    elif pre in coor_carre.keys() and suf in coor_canton.keys():
        if coor_carre[pre][0] > coor_canton[suf][0]:
            seg_pre = Get_nearest_segment_for_carre(suf, True)
            seg_suf = Get_nearest_segment_for_carre(pre, False)
        else:
            seg_pre = Get_nearest_segment_for_carre(pre, True)
            seg_suf = Get_nearest_segment_for_carre(suf, False)
    elif pre in coor_canton.keys() and suf in coor_canton.keys():
        if coor_canton[pre][0] > coor_canton[suf][0]:
            seg_pre = Get_nearest_segment_for_carre(suf, True)
            seg_suf = Get_nearest_segment_for_carre(pre, False)
        else:
            seg_pre = Get_nearest_segment_for_carre(pre, True)
            seg_suf = Get_nearest_segment_for_carre(suf, False)
    else:
        return []

    if ';' in str(jalon):
        jal = jalon.split(';')
        if len(jal) == 3:
            return Find_way(seg_pre, jal[0]) + Find_way(jal[0], jal[1])[1:] + Find_way(jal[1], jal[2])[1:] + Find_way(jal[2], seg_suf)[1:]
        elif len(jal) == 2:
            return Find_way(seg_pre, jal[0]) + Find_way(jal[0], jal[1])[1:] + Find_way(jal[1], seg_suf)[1:]
    else:
        return Find_way(seg_pre, jalon) + Find_way(jalon, seg_suf)[1:]

if __name__ == '__main__':
    
    time1 = time.time()
    path = 'C:/CHAO PAN/Gares/Paris Est/s9c pro/PE_coords.xlsx'
    AdV = pd.read_excel(path, sheet_name = 'AdV')
    zones = pd.read_excel(path, sheet_name = 'zones')
    carres = pd.read_excel(path, sheet_name = 'carres')
    coor = {}
    coor_carre = {}
    coor_jalon = {}
    coor_canton = {}
    for i in range(AdV.shape[0]):
        coor[AdV.loc[i].values[0]] = AdV.loc[i].values[1:]
    for i in range(zones.shape[0]):
        if zones.loc[i].values[0] not in coor.keys():
            coor[zones.loc[i].values[0]] = zones.loc[i].values[1:]
    for i in range(carres.shape[0]):
        if pd.notna(carres.loc[i, 'Shape_Text']):
            carres.loc[i, 'Shape_Text'] = str(carres.loc[i, 'Shape_Text'])
            if carres.iloc[i, 1].isdecimal() or (carres.iloc[i, 1][0] == 'C' and carres.iloc[i, 1][1:].isdecimal()) or (carres.iloc[i, 1][:2] == 'CV' and carres.iloc[i, 1][2:].isdecimal()):
                if ',' not in carres.iloc[i, 1] and 'ZEP' not in carres.iloc[i, 1]:
                    coor_carre[carres.loc[i].values[1]] = carres.loc[i].values[2:]
            if 'jalons' in carres.iloc[i, 0] and ',' not in carres.iloc[i, 1]:
                coor_jalon[carres.loc[i].values[1]] = carres.loc[i].values[2:]
            if 'canton' in carres.iloc[i, 0] and ',' not in carres.iloc[i, 1]:
                coor_canton[carres.loc[i].values[1]] = carres.loc[i].values[2:]

    Set_coordinate()
    
    dis_2_seg = {}
    for i in coor.keys():
        for j in coor.keys():
            if i == j:
                continue
            else:
                d = {}
                d['d00'] = Distance((coor[i][0], coor[i][1]), (coor[j][0], coor[j][1]))
                d['d01'] = Distance((coor[i][0], coor[i][1]), (coor[j][2], coor[j][3]))
                d['d10'] = Distance((coor[i][2], coor[i][3]), (coor[j][0], coor[j][1]))
                d['d11'] = Distance((coor[i][2], coor[i][3]), (coor[j][2], coor[j][3]))
                dis_2_seg[(i, j)] = d

    seg_to_seg = {}
    for i in coor.keys():
        seg_to_seg[i] = Next_segment(i)
    
    '''
    print(All_itinéraire(['2422_C2374']))
    print(Get_nearest_segment_for_carre('C2374', 1), Get_nearest_segment_for_carre('2422', 0))
    print(Find_way('Z_2373', 'Z_2721'))
    print(seg_to_seg['Z_8502'])
    print(math.sqrt(math.pow((coor_carre['2713'][0]+coor_carre['2713'][2]/2 - coor['Z_2716'][0]), 2) + math.pow((coor_carre['2713'][1]+coor_carre['2713'][3]/2 - coor['Z_2716'][1]), 2)))
    print(math.sqrt(math.pow((coor_carre['2713'][0]+coor_carre['2713'][2]/2 - coor['AG_2711a_D'][0]), 2) + math.pow((coor_carre['2713'][1]+coor_carre['2713'][3]/2 - coor['AG_2711a_D'][1]), 2)))
    '''
    
    '''itiName = ReadItiName()
    AllItinéraire = All_itinéraire(itiName)
    export_all_itinéraire(AllItinéraire)
    '''
    forException()
    time2 = time.time()
    print('fini with:', round(float(time2 - time1), 2), 's')









































