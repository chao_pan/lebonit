import pandas as pd
import os
import xlsxwriter
import openpyxl
import datetime
import time

class Incompatibilité():
    def __init__ (self, path_it, path_coor):
        self.allsegments = pd.read_excel(path_it)
        self.path_coor = path_coor
        self.zone_isolée = {}
        self.iti_incom = {} # itinéraire et ses incompatibilités
        self.sig = 0
        self.segmentDeleted = []
        self.itiDirectionAbnormal = []
        self.GetZoneIsonée()
        self.GetItiIncom()
        self.AmeliorateIncom()

    def GetZoneIsonée(self):
        for i in range(self.allsegments.shape[0]):
            self.allsegments.loc[i, self.allsegments.columns[1]] = str(self.allsegments.loc[i, self.allsegments.columns[1]])
            if ';;' in self.allsegments.loc[i, self.allsegments.columns[1]]:
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]].replace(';;', ';')    
            if self.allsegments.loc[i, self.allsegments.columns[1]][0] == ';':
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]][1:]
            if self.allsegments.loc[i, self.allsegments.columns[1]][-1] == ';':
                self.allsegments.loc[i, self.allsegments.columns[1]] = self.allsegments.loc[i, self.allsegments.columns[1]][:-1]
        for i in range(self.allsegments.shape[0]):
            line = []
            for j in self.allsegments.iloc[i, 1].split(';'):
                if 'Z_' in j:
                    if len(j.split('_')) == 4:
                        line.append(j.split('_')[0] + '_' + j.split('_')[1] + '_' + j.split('_')[2])
                    elif len(j.split('_')) == 3:
                        line.append(j.split('_')[0] + '_' + j.split('_')[1])
                    elif len(j.split('_')) == 2:
                        line.append(j)
                    else:
                        print(i+2)
            self.zone_isolée[self.allsegments.iloc[i, 0]] = list(set(line))
    def ISBTLZI(self, itiA, itiB): #Is Superposition Between Two List Zone Isonée
        for zoneisolée in itiA:
            if zoneisolée in itiB:
                return 1 #at least one superposition
                break
        return 0 # no superposition
    def GetItiIncom(self): #First step to get all incompatibities in terms of zone isolée
        for i in range(self.allsegments.shape[0]):
            line = []
            for j in range(self.allsegments.shape[0]):
                len1 = len(self.zone_isolée[self.allsegments.iloc[i, 0]]) + len(self.zone_isolée[self.allsegments.iloc[j, 0]])
                len2 = len(set(self.zone_isolée[self.allsegments.iloc[i, 0]] + self.zone_isolée[self.allsegments.iloc[j, 0]]))
                if self.ISBTLZI(self.zone_isolée[self.allsegments.iloc[i, 0]], self.zone_isolée[self.allsegments.iloc[j, 0]]) == 1:
                    line.append(self.allsegments.iloc[j, 0])
            self.iti_incom[self.allsegments.iloc[i, 0]] = line
    def CNSZI(self, tableau1, tableau2): #count Number Superposition Of Zone Isolée
        self.sig = 0
        for i in tableau1:
            if i in tableau2:
                self.sig += 1
    def FilterSegmentDeleted(self, OneIti, HisIncom):
        self.segmentDeleted = []
        for i in HisIncom:
            if OneIti.split('_')[0] == i.split('_')[1] or OneIti.split('_')[1] == i.split('_')[0]:
                self.CNSZI(self.zone_isolée[OneIti], self.zone_isolée[i])
                if self.sig ==1:
                    if OneIti not in self.itiDirectionAbnormal:
                        if int(OneIti.split('_')[0]) < int(OneIti.split('_')[1]):
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                        else:
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                    else:
                        if int(OneIti.split('_')[0]) < int(OneIti.split('_')[1]):
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])):
                                self.segmentDeleted.append(i)
                        else:
                            if (i not in self.itiDirectionAbnormal and int(i.split('_')[0]) < int(i.split('_')[1])) or \
                                (i in self.itiDirectionAbnormal and int(i.split('_')[0]) > int(i.split('_')[1])):
                                self.segmentDeleted.append(i)                    
    def AmeliorateIncom(self):
        self.VerifyItiDirection()
        for i in self.iti_incom.keys():
            self.FilterSegmentDeleted(i, self.iti_incom[i])
            for j in self.segmentDeleted:
                self.iti_incom[i].remove(j)

    def VerifyItiDirection(self):
        coor = pd.read_excel(self.path_coor, sheet_name = 'carres')
        coor_carre = {}
        itiPrefix = {}
        self.itiDirectionAbnormal = []
        for i in range(coor.shape[0]):
            if pd.notna(coor.loc[i, 'Shape_Text']):
                coor.loc[i, 'Shape_Text'] = str(coor.loc[i, 'Shape_Text'])
                if (coor.iloc[i, 1][0] == 'C' and coor.iloc[i, 1][1:].isdecimal()) or \
                    (coor.iloc[i, 1][:2] == 'CV' and coor.iloc[i, 1][2:].isdecimal()):
                    if ',' not in coor.iloc[i, 1] and 'ZEP' not in coor.iloc[i, 1]:
                        coor_carre[coor.loc[i].values[1]] = coor.loc[i].values[2:]
        for i in range(self.allsegments.shape[0]):
            pre = str(self.allsegments.iloc[i, 0].split('_')[0])
            suf = str(self.allsegments.iloc[i, 0].split('_')[1])
            p = s = ''
            if len(self.allsegments.iloc[i, 0].split('_')) == 3:
                jal = str(self.allsegments.iloc[i, 0].split('_')[2])
            else:
                jal = ''
            for prefix in ['CV', 'C']:
                if p == "": 
                    try:
                        p = coor.loc[coor['Shape_Text'] == prefix + pre, 'Shape_Text'].values[0]
                    except:
                        p = ''
                if s == "":
                    try:
                        s = coor.loc[coor['Shape_Text'] == prefix + suf, 'Shape_Text'].values[0]
                    except:
                        s = ''
            if jal == '':
                if p != '' and s != '':
                    itiPrefix[self.allsegments.iloc[i, 0]] = p + '_' + s
                else:
                    itiPrefix[self.allsegments.iloc[i, 0]] = ''
            else:
                if p != '' and s != '':
                    itiPrefix[self.allsegments.iloc[i, 0]] = p + '_' + s + '_' + jal
                else:
                    itiPrefix[self.allsegments.iloc[i, 0]] = ''
        for i in itiPrefix.keys():
            if itiPrefix[i] != "":
                pre = str(itiPrefix[i].split('_')[0])
                suf = str(itiPrefix[i].split('_')[1])
                preN = int(i.split('_')[0])
                sufN = int(i.split('_')[1])
                if (coor_carre[pre][0] - coor_carre[suf][0])*(preN - sufN) < 0:
                    self.itiDirectionAbnormal.append(i)


class IHM():
    def __init__(self, DIRECTORY, GARE):
        self.directory = DIRECTORY
        self.gare = GARE
        self.zep_grp_it = self.ZEP_GRP_IT()
        self.it_incom = self.IT_INCOM()
        self.liste_zep_grp = self.LISTE_ZEP_GRP()
        self.it_segments = self.IT_SEGMENTS()
        self.zep_segments = self.ZEP_SEGMENTS()
        self.WriteExcel()
    
    def ZEP_GRP_IT(self):    
        fileInput = pd.read_excel('C:/CHAO PAN/Gares/Bordeux/pro/protection_mistral.xlsx')
        zep_grp_it = {}
        for i in range(fileInput.shape[0]):
            line = []
            iti = ''
            for j in range(2, fileInput.shape[1]):
                if pd.notna(fileInput.iloc[i, j]):
                    if len(iti) == 0:
                        iti = str(fileInput.iloc[i, j])
                    else:
                        iti = iti + ';' + str(fileInput.iloc[i, j])
            line.append(fileInput.loc[i, 'Geo'])
            line.append('')
            line.append('')
            line.append(iti)
            zep_grp_it[str(fileInput.loc[i, 'ZEP'])] = line
        return zep_grp_it
    
    def IT_INCOM(self):
        path_it = self.directory+'IT_allsegments_Bordeaux.xlsx'
        path_coor = self.directory+'Bordeaux_coords.xlsx'
        it_incom = Incompatibilité(path_it, path_coor)
        return it_incom.iti_incom
    
    def LISTE_ZEP_GRP(self):
        list_grp = {}
        for xlsx in os.listdir(self.directory + 'S9C/'):
            fileInput = pd.read_excel(self.directory + 'S9C/' + xlsx)
            for i in range(fileInput.shape[0]):
                line = []
                if '+' in str(fileInput.iloc[i, 0]):
                    composition = str(fileInput.iloc[i, 0]).replace('+', ';')
                else:
                    composition = str(fileInput.iloc[i, 0])
                line.append(composition)
                annexe = ''
                line.append(annexe)
                if pd.notna(fileInput.loc[i, 'Protection']):
                    fileInput.loc[i, 'Protection'] = str(fileInput.loc[i, 'Protection'])
                    if '-' in fileInput.loc[i, 'Protection']:
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace('-', ';')
                    if ' -  ' in fileInput.loc[i, 'Protection']:
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace(' -  ', ';')
                    if '\n' in fileInput.loc[i, 'Protection']:
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace('\n', ';')
                    if fileInput.loc[i, 'Protection'][0] == ' ':
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'][1:]
                    if fileInput.loc[i, 'Protection'][-1] == ' ':
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'][:-1]
                    if ' ;  ' in fileInput.loc[i, 'Protection']:
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace(' ;  ', ';')
                    while ('; ' in fileInput.loc[i, 'Protection']):
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace('; ', ';')
                    while (' ;' in fileInput.loc[i, 'Protection']):
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace(' ;', ';')
                    while (';;' in fileInput.loc[i, 'Protection']):
                        fileInput.loc[i, 'Protection'] = fileInput.loc[i, 'Protection'].replace(';;', ';')
                    line.append(fileInput.loc[i, 'Protection'])
                    for pro in fileInput.loc[i, 'Protection'].split(';'):
                        if str(pro) not in self.zep_grp_it.keys():
                            self.zep_grp_it[pro] = ['', '', '', str(pro)]
                else:
                    line.append('')
                line.append('')
                if pd.notna(fileInput.loc[i, 'SG']):
                    line.append(fileInput.loc[i, 'SG'])
                else:
                    line.append('')
                if pd.notna(fileInput.loc[i, 'Page']):
                    line.append(fileInput.loc[i, 'Page'])
                else:
                    line.append('')
                if fileInput.iloc[i, 5] == 'O':
                    ttx = 'authorisé'
                elif fileInput.iloc[i, 5] == 'N':
                    ttx = 'interdit'
                else:
                    ttx = ''
                line.append(ttx)
                if pd.notna(fileInput.loc[i, 'Type']):
                    line.append(fileInput.loc[i, 'Type'])
                else:
                    line.append('')
                if pd.notna(fileInput.iloc[i, 6]):
                    line.append(fileInput.iloc[i, 6])
                else:
                    line.append('')
                if fileInput.iloc[i, 0] in list_grp.keys():
                    suf = '_' + xlsx.split('.')[0][4:]
                    list_grp[str(fileInput.iloc[i, 0]) + suf] = line
                else:
                    list_grp[fileInput.iloc[i, 0]] = line
        return list_grp
    
    def IT_SEGMENTS(self):
        fileInput = pd.read_excel(self.directory + 'IT_allsegments_Bordeaux.xlsx')
        it_segment = {}
        for i in range(fileInput.shape[0]):
            if fileInput.iloc[i, 0] in it_segment.keys():
                print(i+2)
            it_segment[fileInput.iloc[i, 0]] = fileInput.iloc[i, 1]
        return it_segment
    
    def ZEP_SEGMENTS(self):
        segment = {}
        for xlsx in os.listdir(self.directory+'ZEP_segments/'):
            fileInput = pd.read_excel(self.directory+'ZEP_segments/'+xlsx)
            for i in range(fileInput.shape[0]):
                if fileInput.iloc[i, 0] in segment.keys():
                    suf = '_' + xlsx.split('.')[0].split('_')[-1]
                    if pd.notna(fileInput.iloc[i, 1]):
                        segment[str(fileInput.iloc[i, 0])+ suf] = fileInput.iloc[i, 1]
                    else:
                        segment[str(fileInput.iloc[i, 0])+ suf] = ''
                else:
                    if pd.notna(fileInput.iloc[i, 1]):
                        segment[fileInput.iloc[i, 0]] = fileInput.iloc[i, 1]
                    else:
                        segment[fileInput.iloc[i, 0]] = ''
        return segment
    
    def WriteExcel(self):
        wb = xlsxwriter.Workbook(self.directory + '/IHM/IHM_synthese_ZEP_' + gare +'.xlsx')
        ws1 = wb.add_worksheet('Resume')
        ws1.write(0, 0, 'Version')
        ws1.write(0, 1, datetime.datetime.now().strftime("%a, %d %B %Y %H:%M:%S"))
        ws1.protect('hexin')
        
        ws2 = wb.add_worksheet('ZEP+Grp_IT')
        ws2.write(0, 0, 'Protection')
        ws2.write(0, 1, 'Geo')
        ws2.write(0, 2, 'Annexe')
        ws2.write(0, 3, 'Compo')
        ws2.write(0, 4, 'ITs')
        pos = 1
        for pro in self.zep_grp_it.keys():
           ws2.write(pos, 0, pro)
           ws2.write(pos, 1, self.zep_grp_it[pro][0])
           ws2.write(pos, 2, self.zep_grp_it[pro][1])
           ws2.write(pos, 3, self.zep_grp_it[pro][2])
           ws2.write(pos, 4, self.zep_grp_it[pro][3])
           pos += 1
        ws2.protect('hexin')

        ws3 = wb.add_worksheet('Syn_ZEP')
        ws3.write(0, 0, 'M_version')
        ws3.write(0, 1, 'ZEP')
        ws3.write(0, 2, 'ZEP_com')
        k = 0
        for i in self.zep_grp_it.keys():
            ws3.write(0, 2*k+3, 'ZEP' + str(i))
            ws3.write(0, 2*k+4, 'ZEP' + str(i) + '_com')
            ws3.write(k+1, 0, i)
            protection = self.zep_grp_it[i][-1].split(';')
            m = 0
            incom = []
            for j in protection: 
                ws3.write(m+1, 2*k+3, j)
                m += 1
                if j in self.it_incom.keys():
                    incom =  incom + self.it_incom[j]
            count = 0
            incom = set(incom)
            for j in self.it_incom.keys():
                if j not in incom:
                    count = count +1
                    ws3.write(count, 2*k+4, j)
            k += 1
        k = 1
        for iti in self.it_incom.keys():
            ws3.write(k, 2, iti)
            k += 1
        ws3.protect('hexin')
        
        ws4 = wb.add_worksheet('it_ZEP')
        ws4.write(0, 0, 'Its')
        ws4.write(0, 1, 'Incom')
        pos = 1
        for iti in self.it_incom.keys():
            ws4.write(pos, 0, iti)
            line = ''
            for incom in self.it_incom[iti]:
                if len(line) == 0:
                    line = incom
                else:
                    line = line +';' + incom
            ws4.write(pos, 1, line)
            pos += 1
        ws4.protect('hexin')
        
        ws5 = wb.add_worksheet('liste_ZEP_grp')
        ws5.write(0, 0, 'Nom_ZEP')
        ws5.write(0, 1, 'ZEP composition')
        ws5.write(0, 2, 'Annexe')
        ws5.write(0, 3, 'pro clé')
        ws5.write(0, 4, 'Observation')
        ws5.write(0, 5, 'SG')
        ws5.write(0, 6, 'Page')
        ws5.write(0, 7, 'TTx')
        ws5.write(0, 8, 'Type')
        ws5.write(0, 9, 'Zone isolée')
        pos = 1
        for i in self.liste_zep_grp.keys():
            ws5.write(pos, 0, i)
            for j in range(len(self.liste_zep_grp[i])):
                ws5.write(pos, j+1, self.liste_zep_grp[i][j])
            pos += 1
        ws5.protect('hexin')
        
        ws6 = wb.add_worksheet('IT_segments')
        ws6.write(0, 0, 'Itinéraire')
        ws6.write(0, 1, 'Segments')
        pos = 1
        for iti in self.it_segments.keys():
            ws6.write(pos, 0, iti)
            ws6.write(pos, 1, self.it_segments[iti])
            pos += 1
        ws6.protect('hexin')
        
        ws7 = wb.add_worksheet('ZEP_segments')
        ws7.write(0, 0, 'Nom_ZEP')
        ws7.write(0, 1, 'ZEP_Segments')
        pos = 1
        for zep in self.zep_segments.keys():
            ws7.write(pos, 0, zep)
            ws7.write(pos, 1, self.zep_segments[zep])
            pos += 1
        ws7.protect('hexin')
        
        ws8 = wb.add_worksheet('CCTT')
        ws8.write(0, 0, 'Nom_ZEP/Grp')
        ws8.write(0, 1, 'Composition')
        ws8.write(0, 2, 'Annexe')
        ws8.write(0, 3, 'Protection')
        ws8.write(0, 4, 'Observation')
        ws8.write(0, 5, 'SG')
        ws8.protect('hexin')
        
        wb.close()
        
if __name__ == '__main__':
    time_begin = time.time()
    directory = 'C:/CHAO PAN/Gares/Bordeux/'
    gare = 'Bordeaux'
    test = IHM(directory, gare)
    time_end = time.time()
    print('finished with {}'.format(round(time_end-time_begin, 2)))
















































